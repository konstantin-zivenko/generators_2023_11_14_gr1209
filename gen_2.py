def infinity_seq():
    num = 0
    while True:
        yield num
        num += 1
