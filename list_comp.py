a = [1, 2, 4, 6, 8]
aa = []
for i in a:
    if not i % 2:
        aa.append(i ** 2)

bb = [i ** 2 for i in a if not i % 2]
bb_s = {i: i ** 2 for i in a if not i % 2}
bb_gen = (i ** 2 for i in a if not i % 2)

if __name__ == "__main__":
    print(aa)
    print(bb)
    print(bb_s)
    print(bb_gen)

