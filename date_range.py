from datetime import date, timedelta


def daterange(
        start_stop: date,
        stop: date = None,
        step: int = 1
):
    step_interval = timedelta(days=step)
    if not stop:
        start_date = date.today()
        stop_date = start_stop
    else:
        start_date = start_stop
        stop_date = stop

    current_date = start_date
    while current_date <= stop_date:
        yield current_date
        current_date += step_interval


if __name__ == "__main__":
    start = date(2023, 11, 15)
    stop = date(2023, 11, 25)
    for day in daterange(stop, step=3):
        print(day)

