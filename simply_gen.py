from palindrom import is_palindrome


def simply_infinity_palindromes():
    num = 0
    while True:
        if is_palindrome(num):
            yield num
        num += 1


# .send()
def infinity_palindromes():
    num = 0
    while True:
        if is_palindrome(num):
            i = (yield num)
            if i is not None:
                num = i
        num += 1


if __name__ == "__main__":
    # for i in simply_infinity_palindromes():
    #     print(i)
    # pal_gen = infinity_palindromes()
    # for i in pal_gen:
    #     print(i)
    #     digits = len(str(i))
    #     pal_gen.send(10 ** digits)

# .throw()
#     pal_gen = infinity_palindromes()
#     for i in pal_gen:
#         print(i)
#         digits = len(str(i))
#         if digits == 8:
#             pal_gen.throw(ValueError("We don't like long palindromes..."))
#         pal_gen.send(10 ** digits)

# close()
    pal_gen = infinity_palindromes()
    for i in pal_gen:
        print(i)
        digits = len(str(i))
        if digits == 8:
            pal_gen.close()
        pal_gen.send(10 ** digits)
